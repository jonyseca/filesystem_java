# Basic filesystem - João Fonseca

## Descrição do Projeto

* Aplicação JAVA desenvolvida com o intuito de simular um filesystem e a manipulação dos ficheiros que o constituem.
* Inspirado em UNIX, tem uma UI na consula e é possível navegar dentro do filesystem, selecionado o ficheiro e diretórios à escolha.
* Desenvolvido através de DDD (Domain Driven Design)
* Não tem persistência, porém da maneira que foi desenvolvido (com entidades e value objects), é de fácil implemetação no futuro.
* Contém testes unitários relacionados com o comportamento dos objetos.
* Projeto Maven - para dar build correr *mvn package* - vem já com o último build no repositório.

## Modelo de Domínio

![Modelo](basicFilesystem.jpg)

## Comandos

* Listar conteúdos do diretório (nomes dos ficheiros).
  * **ls**
    * **ls $dir**

* Listar conteúdos do diretório (nomes e detalhes dos ficheiros).
  * **lsdetails**
    * **lsdetails $dir**

* Criar ficheiro no diretório atual.
  * **touch $nomeFile**

* Criar diretório no diretório atual.
  * **mkdir $nomeDir**

* Remover ficheiro no diretório atual.
  * **rm $file**

* Remover diretório no diretório atual.
  * **rmdir $dir**

* Mudança de diretório
  * **cd ..** - Anterior
  * **cd $dir**

* Mover ficheiro de diretório para diretório
  * **copy $file $dir**

* Copiar ficheiro de diretório para diretório
  * **move $file $dir**

* Mudar permissões do ficheiro
  * **chmod read=$bool write=$bool exec=$bool $file"**

* Editar conteudo ficheiro
  * **editcontent $file $contents**

* Listar conteudo do ficheiro
  * **cat $file**


Todos os comandos estão dependentes das permissões que os ficheiros têm definidas.



## Observações

* Podia ter sido construida uma árvore que iria representar o filesystem em vez uma classe com a root. Isto evitaria a duplicação de dados(diretório atual tem o pai e os filhos) e a pesquisa, inserção e remoção seriam mais eficientes - **O(n * log(n))** x **O(n^2)**

* Dado que não existe um sistema de utilizadores, as permissões funcionam singularmente e não organizadas por grupo como no UNIX(user, group, others). Assim, cada ficheiro tem apenas um conjunto de permissões.
